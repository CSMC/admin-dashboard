var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning()

    .addEntry('js/main', './assets/js/main.js')


    .addEntry('js/fastclick','fastclick/lib/fastclick.js')
    .addEntry('js/nprogress','nprogress/nprogress.js')

    .addEntry('js/gentelella','gentelella/build/js/custom.js')

    //<!-- Datatables -->
    .addEntry('js/jquery.dataTables.min','gentelella/vendors/datatables.net/js/jquery.dataTables.min.js')
    .addEntry('js/dataTables.bootstrap.min','gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')
    .addEntry('js/dataTables.buttons.min','gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')
    .addEntry('js/buttons.flash.min','gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js')
    .addEntry('js/buttons.html5.min','gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js')
    .addEntry('js/buttons.print.min','gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js')
    .addEntry('js/dataTables.fixedHeader.min','gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')
    .addEntry('js/dataTables.keyTable.min.min','gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')
    .addEntry('js/dataTables.responsive.min','gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')
    .addEntry('js/responsive.bootstrap','gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')


    ///////////
    // FORM - GUI EDITOR CONTROL :
    .addEntry('js/bootstrap-progressbar.min','gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')
    .addEntry('js/icheck.min', 'gentelella/vendors/iCheck/icheck.min.js')
    .addEntry('js/moment.min', 'gentelella/vendors/moment/min/moment.min.js')
    .addEntry('js/daterangepicker', 'gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js')
    .addEntry('js/jquery.hotkeys','gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js')
    .addEntry('js/prettify','gentelella/vendors/google-code-prettify/src/prettify.js')
    .addEntry('js/jquery.tagsinput','gentelella/vendors/jquery.tagsinput/src/jquery.tagsinput.js')
    .addEntry('js/switchery.min','gentelella/vendors/switchery/dist/switchery.min.js')
    .addEntry('js/select2.full.min','gentelella/vendors/select2/dist/js/select2.full.min.js')
    .addEntry('js/parsley.min','gentelella/vendors/parsleyjs/dist/parsley.min.js')
    .addEntry('js/autosize.min','gentelella/vendors/autosize/dist/autosize.min.js')
    .addEntry('js/starrr','gentelella/vendors/starrr/dist/starrr.js')
    .addEntry('js/jquery.autocomplete.min','gentelella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')
    .addEntry('js/bootstrap-wysiwyg.min','gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')
    ///////////


    .addEntry('js/dataTables.scroller.min','gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')

    .addEntry('js/jszip.min','gentelella/vendors/jszip/dist/jszip.min.js')
    .addEntry('js/pdfmake.min','gentelella/vendors/pdfmake/build/pdfmake.min.js')
    .addEntry('js/vfs_fonts','gentelella/vendors/pdfmake/build/vfs_fonts.js')



    //.addEntry('js/dataTables.bootstrap.min','gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')



    .addStyleEntry('css/global', './assets/css/global.scss')

    
    .enableSassLoader()

;

module.exports = Encore.getWebpackConfig();