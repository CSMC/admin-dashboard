<?php
namespace App\DataFixtures;

use App\Entity\Course\Course;
use App\Entity\User\Role;
use App\Entity\Course\Department;
use App\Entity\User\User;
use App\Entity\User\userGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $department = new Department();
        $department->setAbbreviation('SE');
        $department->setName('Software Engineer');
        $department->setAdminNotes('SE notes');
        $department->setLastModifiedOn()->setCreatedOn();

        $manager->persist($department);

        // create 20 products! Bam!
        for ($i = 0; $i < 20; $i++) {
            $course = new Course();
            $course->setName('course'.$i);
            $course->setDepartment($department);
            $course->setSupported(true);
            $course->setNumber($i);
            $course->setLastModifiedOn()->setCreatedOn();
            $manager->persist($course);
        }


        // create several roles to the database
        $student = new Role();
        $mentor = new Role();
        $instructor = new Role();
        $admin = new Role();



        $student->setName('student');
        $mentor->setName('mentor');
        $instructor->setName('instructor');
        $admin->setName('admin');


        $student->setDescription('this is a student');
        $mentor->setDescription('this is a mentor');
        $instructor->setDescription('this is an instructor');
        $admin->setDescription('this is an admin');

        $student->setLastModifiedOn()->setCreatedOn();
        $mentor->setLastModifiedOn()->setCreatedOn();
        $instructor->setLastModifiedOn()->setCreatedOn();
        $admin->setLastModifiedOn()->setCreatedOn();


        $manager->persist($student);
        $manager->persist($mentor);
        $manager->persist($instructor);
        $manager->persist($admin);



        //create user data(first name, last name, username(netid), roles,
        for ($i = 1; $i <= 5 ; $i++) {
            $user = new User();
            $user->setFirstName('FirstName'.$i);
            $user->setLastName('FirstName'.$i);
            $user->setUserName('NetID'.$i);
            $user->addRole($student);
            $user->addRole($mentor);
            $manager->persist($user);
        }

        for ($i = 6; $i <= 10 ; $i++) {
            $user = new User();
            $user->setFirstName('FirstName'.$i);
            $user->setLastName('FirstName'.$i);
            $user->setUserName('NetID'.$i);
            $user->addRole($mentor);
            $manager->persist($user);
        }

        for ($i = 11; $i <= 15  ; $i++) {
            $user = new User();
            $user->setFirstName('FirstName'.$i);
            $user->setLastName('FirstName'.$i);
            $user->setUserName('NetID'.$i);
            $user->addRole($instructor);
            $manager->persist($user);
        }

        for ($i = 16; $i <= 20 ; $i++) {
            $user = new User();
            $user->setFirstName('FirstName'.$i);
            $user->setLastName('FirstName'.$i);
            $user->setUserName('NetID'.$i);
            $user->addRole($admin);
            $manager->persist($user);
        }


        // create several user group to the database
        $studentGroup = new userGroup();
        $mentorGroup = new userGroup();
        $instructorGroup = new userGroup();
        $adminGroup = new userGroup();



        $studentGroup->setName('student user group');
        $mentorGroup->setName('mentor user group');
        $instructorGroup->setName('instructor user group');
        $adminGroup->setName('admin user group');


        $studentGroup->setDescription('this is a student user group');
        $mentorGroup->setDescription('this is a mentor user group');
        $instructorGroup->setDescription('this is an instructor user group');
        $adminGroup->setDescription('this is an admin user group');

        $studentGroup->setLastModifiedOn()->setCreatedOn();
        $mentorGroup->setLastModifiedOn()->setCreatedOn();
        $instructorGroup->setLastModifiedOn()->setCreatedOn();
        $adminGroup->setLastModifiedOn()->setCreatedOn();


        $manager->persist($studentGroup);
        $manager->persist($mentorGroup);
        $manager->persist($instructorGroup);
        $manager->persist($adminGroup);




        $manager->flush();
    }
}