<?php

namespace App\Entity\User;

use App\Entity\User\Info\Info;
use App\Entity\User\Role;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\Validator\Constraints as Assert;




/**
 * * @ORM\Entity
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="card_idx", columns={"card_id"})})
 *
 * @Serializer\ExclusionPolicy("all")
 *

 * @UniqueEntity(
 *     fields = {"username"},
 *     message = "This netID {{ value }} already exists."
 * )

 */

class User implements UserInterface, \Serializable {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="first_name", length=32, nullable=true)
     *
     *@Assert\Length(
     *      min = 1,
     *      max = 32,
     *      maxMessage = "The first name cannot be longer than {{ limit }} characters"
     * )
     *
     *
     * @Serializer\Expose()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", length=64, nullable=true)
     *
     *     *@Assert\Length(
     *      min = 1,
     *      max = 64,
     *      maxMessage = "The last name cannot be longer than {{ limit }} characters"
     * )
     *
     * @Serializer\Expose()
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", name="netid", length=9, unique=true)
     *
     *
     *@Assert\Length(
     *      min = 3,
     *      max = 9,
     *      maxMessage = "The netID cannot be longer than {{ limit }} characters"
     * )
     *
     * @Serializer\Expose()
     */
    private $username;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\Info\Info", mappedBy="user")
     *
     * @Serializer\Expose()
     */
    private $info;

    /**
     * @ORM\Column(type="string", name="scancode", length=16, unique=true, nullable=true)
     *
     *@Assert\Length(
     *      min = 0,
     *      max = 16,
     *      maxMessage = "The scan code need to be exactly {{ limit }} characters"
     * )
     */
    private $scancode;



    /**
     * @ORM\Column(type="string", name="card_id", length=9, nullable=true, options={"comment":"Card ID as given by scanner"})
     *  *@Assert\Length(
     *      min = 0,
     *      max = 9,
     *      maxMessage = "The card id should be exactly {{ limit }} characters"
     * )
     *
     *
     */
    private $cardId;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_roles")
     *
     * @Serializer\Expose()
     */
    private $roles;


    // @ORM\JoinTable(name="user_userGroup")

    /**
     * @ORM\ManyToMany(targetEntity="UserGroup", inversedBy="users")
     *
     * @Serializer\Expose()
     */
    private $userGroups;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Misc\Visit", mappedBy="user", cascade={"persist"})
     */
    private $visits;

    /**
     * Constructor
     */
    public function __construct() {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set scancode
     *
     * @param string $scancode
     *
     * @return User
     */
    public function setScancode($scancode) {
        $this->scancode = $scancode;

        return $this;
    }

    /**
     * Get scancode
     *
     * @return string
     */
    public function getScancode() {
        return $this->scancode;
    }


    /**
     * Set cardId
     *
     * @param string $cardId
     *
     * @return User
     */
    public function setCardId($cardId) {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId
     *
     * @return string
     */
    public function getCardId() {
        return $this->cardId ;
    }

    /**
     * Add role
     *
     * @param \App\Entity\User\Role $role
     *
     * @return User
     */
    public function addRole(\App\Entity\User\Role $role) {
        $this->roles [] = $role;

        return $this;
    }


    /**
     * Remove role
     *
     * @param \App\Entity\User\Role $role
     */
    public function removeRole(\App\Entity\User\Role $role) {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * Returns role strings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles() {
        return $this->roles;
    }



    public function addUserGroup(\App\Entity\User\UserGroup $ug) {
        $this->userGroups [] = $ug;

        return $this;
    }

    /**
     * Remove userGroups
     *
     * @param \App\Entity\User\UserGroup
     */
    public function removeUserGroup(\App\Entity\User\UserGroup $ug) {
        $this->userGroups->removeElement($ug);
    }

    /**
     * Get userGroups
     *
     * Returns userGroups strings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups() {
        return $this->userGroups;
    }

    /**
     * Set info
     *
     * @param \App\Entity\User\Info\Info $info
     *
     * @return User
     */
    public function setInfo(\App\Entity\User\Info\Info $info = null) {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return \App\Entity\User\Info\Info
     */
    public function getInfo() {
        return $this->info;
    }


    /**
     * Add visit
     *
     * @param \App\Entity\Misc\Visit $visit
     *
     * @return User
     */
    public function addVisit(\App\Entity\Misc\Visit $visit) {
        $this->visits[] = $visit;

        return $this;
    }

    /**
     * Remove visit
     *
     * @param \App\Entity\Misc\Visit $visit
     */
    public function removeVisit(\App\Entity\Misc\Visit $visit) {
        $this->visits->removeElement($visit);
    }

    /**
     * Get visits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisits() {
        return $this->visits;
    }

    public function getEmail() {
        return ($this->info && $this->info->getEmail())
            ? $this->info->getEmail()
            : ($this->username . '@utdallas.edu');
    }

    public function __toString() {
        // return $this->getUsername() . ' ' . $this->getFirstName() . ' ' . $this->getLastName();
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function serialize() {
        return json_encode(
            array(
                $this->id,
                $this->firstName,
                $this->lastName,
                $this->username,
                $this->scancode,
                $this->roles,
                $this->userGroups,
            ));
    }

    public function unserialize($serialized) {
        list ($this->id, $this->firstName, $this->lastName, $this->username, $this->scancode,
            $this->roles,
            $this->userGroups) = json_decode(
            $serialized);
    }

    /**
     * Unused
     */
    public function getPassword() {
        return null;
    }

    /**
     * Unused
     *
     * Must return null
     */
    public function getSalt() {
        return null;
    }

    /**
     * Unused
     *
     * Must return null
     */
    public function eraseCredentials() {
        return null;
    }
}
