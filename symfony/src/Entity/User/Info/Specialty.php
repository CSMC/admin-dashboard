<?php

namespace App\Entity\User\Info;

use Doctrine\ORM\Mapping as Orm;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_specialty")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Specialty {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Info", inversedBy="specialties")
     * @ORM\JoinColumn(name="info_id", referencedColumnName="id")
     */
    private $info;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Misc\Subject")
     * @ORM\JoinColumn(name="specialty_topic_id", referencedColumnName="id")
     *
     * @Serializer\Expose()
     */
    private $topic;

    /**
     * @ORM\Column(type="integer", name="rating", length=1)
     *
     * @Serializer\Expose()
     */
    private $rating;

    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Specialty
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set info
     *
     * @param \App\Entity\User\Info\Info $info
     *
     * @return Specialty
     */
    public function setInfo(\App\Entity\User\Info\Info $info = null)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return \App\Entity\User\Info\Info
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set topic
     *
     * @param \App\Entity\Misc\Subject $topic
     *
     * @return Specialty
     */
    public function setTopic(\App\Entity\Misc\Subject $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \App\Entity\Misc\Subject
     */
    public function getTopic()
    {
        return $this->topic;
    }
}
