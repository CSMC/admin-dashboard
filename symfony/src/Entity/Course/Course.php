<?php

namespace App\Entity\Course;

use App\Entity\Interfaces\ModifiableInterface;
use App\Entity\Traits\ModifiableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Represents a course that is offered.
 *
 * @ORM\Entity
 * @ORM\Table(name="course")
 * @UniqueEntity(
 *     fields = {"name"},
 *     message = "Course {{ value }} already exists."
 * )
 * @UniqueEntity(
 *     fields = {"number", "department"},
 *     message = "Course #{{ value }} and the department already exists."
 * )
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @Serializer\VirtualProperty(
 *     "department",
 *     exp="object.getDepartment().getId()"
 * )
 */



class Course implements ModifiableInterface {
    use ModifiableTrait;
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="courses")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * @Assert\Length(
     *      max = 64,
     *      maxMessage = "The Course name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", length=64, name="name", unique=true)
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @Assert\Length(
     *      min = 4,
     *      max = 4,
     *      exactMessage = "The length of Course number should be exactly {{ limit }}.",
     * )
     *
     *
     * @ORM\Column(type="string", length=4, name="number")
     *
     * @Serializer\Expose()
     */
    private $number;

    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="course")
     *
     * @Serializer\Expose()
     */
    private $sections;

    /**
     * @ORM\Column(type="boolean", name="supported")
     *
     * @Serializer\Expose()
     */
    private $supported;


    /**
     *
     * @Assert\Length(
     *      max = 8192,
     *      maxMessage = "The description cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", name="description", length=8192, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $description;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Returns the course's id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the course's name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the course's name
     *
     * @param string $name
     *
     * @return Course
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the course's number
     *
     * @return string
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     * Sets the course's number
     *
     * Number is typically 4 digits, e.g. 1336
     *
     * @param string $number
     *
     * @return Course
     */
    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

    /**
     * Returns the course's department
     *
     * @return \App\Entity\Course\Department
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Sets the course's department
     *
     * @param \App\Entity\Course\Department $department
     *
     * @return Course
     */
    public function setDepartment(\App\Entity\Course\Department $department = null) {
        $this->department = $department;

        return $this;
    }

    /**
     * Adds a section to the course
     *
     * @param \App\Entity\Course\Section $section
     *
     * @return Course
     */
    public function addSection(\App\Entity\Course\Section $section) {
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Removes a section from the course
     *
     * @param \App\Entity\Course\Section $section
     */
    public function removeSection(\App\Entity\Course\Section $section) {
        $this->sections->removeElement($section);
    }

    /**
     * Returns the course's sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections() {
        return $this->sections;
    }

    /**
     * Set supported
     *
     * @param boolean $supported
     *
     * @return Course
     */
    public function setSupported($supported)
    {
        $this->supported = $supported;

        return $this;
    }

    /**
     * Get supported
     *
     * @return boolean
     */
    public function getSupported()
    {
        return $this->supported;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return Course
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }





}
