<?php

namespace App\Entity\Schedule;

use App\Entity\Interfaces\ModifiableInterface;
use App\Entity\Traits\ModifiableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Represents the parameters for the calender
 *
 *

 * @ORM\Entity
 * @ORM\Table(name="schedule_time")
 */
class ScheduleTime implements ModifiableInterface
{
    use ModifiableTrait;


    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="date", name="startDate")
     */
    private $startDate;

    /**
     *
     * @ORM\Column(type="date", name="endDate")
     */
    private $endDate;


    /**
     * @ORM\Column(type="time", name="startTime")
     */
    private $startTime;
    /**
     * @ORM\Column(type="time", name="endTime")
     */
    private $endTime;
    /**
     *
     * @ORM\Column(type="string", name="startOfWeek")
     */
    private $startOfWeek;




    /**
     * Returns the id of the calender parameter
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the start date for the calender
     *
     * @return datetime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the start date for the calender
     *
     * @param string $startDate
     *
     * @return ScheduleTime
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the start day of the week for the calender
     *
     * @return string
     */
    public function getStartOfWeek()
    {
        return $this->startOfWeek;
    }

    /**
     * Sets the start day of the week for the calender
     *
     * @param string $startOfWeek
     *
     * @return ScheduleTime
     */
    public function setStartOfWeek($startofWeek)
    {
        $this->startOfWeek = $startofWeek;
    }





    /**
     * Returns the end date for the calender
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Sets the end date for the calender
     *
     * @param string $endDate
     *
     * @return ScheduleTime
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }


    /**
     * Returns the start time of the day for the calender
     *
     * @return datetime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Sets the start time of the day for calender
     *
     * @param string $startTime
     *
     * @return ScheduleTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }


    /**
     * Returns the end time of the day for calender
     *
     * @return datetime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }
    /**
     * Sets the end time of the day for calender
     *
     * @param string $endTime
     *
     * @return ScheduleTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }
}

