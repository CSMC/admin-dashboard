<?php

namespace App\Entity\Schedule;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="schedule")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Schedule {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Shift", mappedBy="schedule")
     *
     * @Serializer\Expose()
     */
    private $shifts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Schedule\ScheduledShift", mappedBy="schedule")
     */
    private $scheduledShifts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Misc\Semester", inversedBy="schedule")
     * @ORM\JoinColumn(name="semester_id", referencedColumnName="id")
     */
    private $semester;

    /**
     * Constructor
     */
    public function __construct() {
        $this->shifts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return guid
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add shift
     *
     * @param \App\Entity\Schedule\Shift $shift
     *
     * @return Schedule
     */
    public function addShift(\App\Entity\Schedule\Shift $shift) {
        $this->shifts[] = $shift;

        return $this;
    }

    /**
     * Remove shift
     *
     * @param \App\Entity\Schedule\Shift $shift
     */
    public function removeShift(\App\Entity\Schedule\Shift $shift) {
        $this->shifts->removeElement($shift);
    }

    /**
     * Get shifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShifts() {
        return $this->shifts;
    }

    /**
     * Set semester
     *
     * @param \App\Entity\Misc\Semester $semester
     *
     * @return Schedule
     */
    public function setSemester(\App\Entity\Misc\Semester $semester = null) {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return \App\Entity\Misc\Semester
     */
    public function getSemester() {
        return $this->semester;
    }

    /**
     * Add scheduledShift
     *
     * @param \App\Entity\Schedule\ScheduledShift $scheduledShift
     *
     * @return Schedule
     */
    public function addScheduledShift(\App\Entity\Schedule\ScheduledShift $scheduledShift)
    {
        $this->scheduledShifts[] = $scheduledShift;

        return $this;
    }

    /**
     * Remove scheduledShift
     *
     * @param \App\Entity\Schedule\ScheduledShift $scheduledShift
     */
    public function removeScheduledShift(\App\Entity\Schedule\ScheduledShift $scheduledShift)
    {
        $this->scheduledShifts->removeElement($scheduledShift);
    }

    /**
     * Get scheduledShifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScheduledShifts()
    {
        return $this->scheduledShifts;
    }
}
