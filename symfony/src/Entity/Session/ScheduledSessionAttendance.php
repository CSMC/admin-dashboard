<?php

namespace App\Entity\Session;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="scheduled_session_attendance")
 */
class ScheduledSessionAttendance extends SessionAttendance {
    /**
     * @ORM\ManyToOne(targetEntity="Timeslot", inversedBy="attendances")
     * @ORM\JoinColumn(name="timeslot_id", referencedColumnName="id")
     */
    private $timeslot;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Misc\Swipe")
     * @ORM\JoinTable(name="scheduled_session_swipes",
     *     joinColumns={@ORM\JoinColumn(name="attendance_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="swipe_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $swipes;

    /**
     * Set timeslot
     *
     * @param \App\Entity\Session\Timeslot $timeslot
     *
     * @return ScheduledSessionAttendance
     */
    public function setTimeslot(\App\Entity\Session\Timeslot $timeslot = null) {
        $this->timeslot = $timeslot;

        return $this;
    }

    /**
     * Get timeslot
     *
     * @return \App\Entity\Session\Timeslot
     */
    public function getTimeslot() {
        return $this->timeslot;
    }

    /**
     * Add swipe
     *
     * @param \App\Entity\Misc\Swipe $swipe
     *
     * @return ScheduledSessionAttendance
     */
    public function addSwipe(\App\Entity\Misc\Swipe $swipe) {
        $this->swipes[] = $swipe;

        return $this;
    }

    /**
     * Remove swipe
     *
     * @param \App\Entity\Misc\Swipe $swipe
     */
    public function removeSwipe(\App\Entity\Misc\Swipe $swipe) {
        $this->swipes->removeElement($swipe);
    }

    /**
     * Get swipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSwipes() {
        return $this->swipes;
    }
}
