<?php

namespace App\Entity\Session;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Session\QuizRepository")
 * @ORM\Table(name="quiz")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Quiz extends Session {
    /**
     * @ORM\Column(type="date", name="start_date")
     *
     * @Serializer\Expose()
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", name="end_date")
     *
     * @Serializer\Expose()
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\Misc\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     *
     * @Serializer\Expose()
     */
    private $room;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Session\QuizAttendance", mappedBy="quiz", cascade={"remove"})
     */
    private $attendances;

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Quiz
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Quiz
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set room
     *
     * @param \App\Entity\Misc\Room $room
     *
     * @return Quiz
     */
    public function setRoom(\App\Entity\Misc\Room $room = null) {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \App\Entity\Misc\Room
     */
    public function getRoom() {
        return $this->room;
    }

    /**
     * Add attendance
     *
     * @param \App\Entity\Session\QuizAttendance $attendance
     *
     * @return Quiz
     */
    public function addAttendance(\App\Entity\Session\QuizAttendance $attendance) {
        $this->attendances[] = $attendance;

        return $this;
    }

    /**
     * Remove attendance
     *
     * @param \App\Entity\Session\QuizAttendance $attendance
     */
    public function removeAttendance(\App\Entity\Session\QuizAttendance $attendance) {
        $this->attendances->removeElement($attendance);
    }

    /**
     * Get attendances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendances() {
        return $this->attendances;
    }
}
