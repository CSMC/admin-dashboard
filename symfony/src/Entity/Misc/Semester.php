<?php

namespace App\Entity\Misc;

use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="semester", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="UQ_semester_season_year", columns={"season_id", "year"})
 *     })
 *
 * @UniqueEntity(
 *     fields={"season", "year"},
 *     message="Semester already exists")
 *
 * * @Serializer\VirtualProperty(
 *     "semester_season",
 *     exp="object.getSemesterSeason().getId()"
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Semester {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SemesterSeason", inversedBy="semesters")
     * @ORM\JoinColumn(name="season_id", referencedColumnName="id")
     */
    private $season;

    /**
     * @ORM\Column(type="integer", name="year")
     */
    private $year;

    /**
     * @ORM\Column(type="date", name="start_date", nullable=true)
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", name="end_date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean", name="active")
     *
     */
    private $active;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Schedule\Schedule", mappedBy="semester")
     */
    private $schedule;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation() {
        return $this->season->getPrefix() . substr(((string)$this->year), -2);
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Semester
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }



    /**
     * Returns the semester's season
     *
     * @return \App\Entity\Misc\SemesterSeason
     */
    public function getSeason() {
        return $this->season;
    }

    /**
     * Sets the course's department
     *
     * @param \App\Entity\Misc\SemesterSeason $season
     *
     * @return Semester
     */
    public function setSeason(\App\Entity\Misc\SemesterSeason $season) {
        $this->season = $season;

        return $this;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Semester
     */
    public function setYear($year) {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Semester
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Semester
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set schedule
     *
     * @param \App\Entity\Schedule\Schedule $schedule
     *
     * @return Semester
     */
    public function setSchedule(\App\Entity\Schedule\Schedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \App\Entity\Schedule\Schedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }
}