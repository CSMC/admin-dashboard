<?php

namespace App\Entity\Misc;


use App\Entity\Interfaces\ModifiableInterface;
use App\Entity\Traits\ModifiableTrait;
use Doctrine\ORM\Mapping as ORM;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="announcement")
 * @UniqueEntity(
 *     fields = {"subject", "startDate", "endDate"},
 *     message = "The subject, {{ value }}, startDate, and endDate already exists."
 * )
 */
class Announcement implements ModifiableInterface {
    use ModifiableTrait;
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\Role")
     * @ORM\JoinTable(name="announcement_roles",
     *      joinColumns={@ORM\JoinColumn(name="announcement_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     */
    private $roles;


    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\User")
     * @ORM\JoinTable(name="announcement_users",
     *      joinColumns={@ORM\JoinColumn(name="announcement_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $users;



    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\UserGroup")
     * @ORM\JoinTable(name="announcement_userGroups",
     *      joinColumns={@ORM\JoinColumn(name="announcement_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="userGroup_id", referencedColumnName="id")}
     *      )
     */
    private $userGroups;


    /**
     *
     * @Assert\Length(
     *      max = 256,
     *      maxMessage = "The subject cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", name="subject", length=256, unique=true)
     */
    private $subject;


    /**
     *
     * @Assert\Length(
     *      max = 8192,
     *      maxMessage = "The message cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", name="message", length=8192)
     */
    private $message;


    /**
     * @ORM\Column(type="date", name="post_date")
     */
    private $postDate;


    /**
     * @ORM\Column(type="date", name="start_date")
     */
    private $startDate;


    /**
     * @ORM\Column(type="date", name="end_date")
     */
    private $endDate;


    /**
     * @ORM\Column(type="integer", name="duration", nullable=true)
     */
    private $duration;


    /**
     * @ORM\Column(type="boolean", name="active", nullable=true)
     */
    private $active;


    /**
     * Constructor
     */
    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();

        $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();

        $this->startDate = new \DateTime();
        $this->endDate = new \DateTime('now +1 month');

    }


/*

    public function serialize() {
        return json_encode(
            array(
                $this->id,
                $this->users,
                // $this->roles,
                $this->userGroups,
                $this->subject,
                $this->message,
                $this->postDate,
                $this->startDate,
                $this->endDate,
                $this->duration,
                $this->active
            ));
    }

*/

/*
    public function unserialize($serialized) {
        list ($this->id,
            $this->users,
            // $this->roles,
            $this->userGroups,
            $this->subject,
            $this->message,
            $this->postDate,
            $this->startDate,
            $this->endDate,
            $this->duration,
            $this->active
            ) = json_decode($serialized);
    }

*/


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Announcement
     */
    public function setSubject($subject) {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }




    /**
     * Set message
     *
     * @param string $message
     *
     * @return Announcement
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Set postDate
     *
     * @param \DateTime $postDate
     *
     * @return Announcement
     */
    public function setPostDate($postDate) {
        $this->postDate = $postDate;

        return $this;
    }

    /**
     * Get postDate
     *
     * @return \DateTime
     */
    public function getPostDate() {
        return $this->postDate;
    }


    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }


    /**
     * Set startDate
     *
     * @param \DateTime $postDate
     *
     * @return Announcement
     */
    public function setStartDate($startDate) {
        $this->startDate= $startDate;

        return $this;
    }


    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }


    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Announcement
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }






    /**
     * Add role
     *
     * @param \App\Entity\User\Role $role
     *
     * @return Announcement
     */
    public function addRole(\App\Entity\User\Role $role) {
        $this->roles[] = $role;

        return $this;
    }


    /**
     * Remove role
     *
     * @param \App\Entity\User\Role $role
     */
    public function removeRole(\App\Entity\User\Role $role) {
        $this->roles->removeElement($role);
    }



    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */

    public function getRoles() {
        return $this->roles;
    }


    /**
     * Add user
     *
     * @param \App\Entity\User\User $user
     *
     * @return Announcement
     */
    public function addUser(\App\Entity\User\User $user) {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \App\Entity\User\User $user
     */
    public function removeUser(\App\Entity\User\User $user) {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers() {
        return $this->users;
    }




    /**
     * Add userGroup
     *
     * @param \App\Entity\User\UserGroup $userGroup
     *
     * @return Announcement
     */
    public function addUserGroup(\App\Entity\User\UserGroup $userGroup) {
        $this->userGroups[] = $userGroup;

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \App\Entity\User\UserGroup $userGroup
     */
    public function removeUserGroup(\App\Entity\User\UserGroup $userGroup) {
        $this->userGroups->removeElement($userGroup);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups() {
        return $this->userGroups;
    }

    /**
     *
     * Set UserGroups null
     *
     * EXCLUSIVE selection on Roles when both Roles and UserGroups are selected
     *
     * @return Announcement
     *
     */
    public function setUserGroupsNull() {
        $this->userGroups = null;

        return $this;
    }



    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Announcement
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Announcement
     */
    public function setDuration($duration) {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration() {
        return $this->duration;
    }
}
