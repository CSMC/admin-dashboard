<?php

namespace App\Entity\Misc;

use App\Entity\Interfaces\ModifiableInterface;
use App\Entity\Traits\ModifiableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Represents the operational Hours for the mentoring
 *
 *

 * @ORM\Entity
 * @ORM\Table(name="operational_hours")
 */
class OperationalHours implements ModifiableInterface
{
    use ModifiableTrait;


    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", name="day")
     */
    private $day;


    /**
     * @ORM\Column(type="time", name="startTime")
     */
    private $startTime;
    /**
     * @ORM\Column(type="time", name="endTime")
     */
    private $endTime;




    /**
     * Returns the opeational hour's id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the day of operation
     *
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Sets the day of operation
     *
     * @param string $day
     *
     * @return OperationalHours
     */
    public function setDay($day)
    {
        $this->day = $day;
    }




    /**
     * Returns the start time for the day of operation
     *
     * @return time
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Sets the start time for the day of operation
     *
     * @param string $startTime
     *
     * @return OperationalHours
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }


    /**
     * Returns the end time for the day of operation
     *
     * @return time
     */
    public function getEndTime()
    {
        return $this->endTime;
    }
    /**
     * Sets the end time for the day of operation
     *
     * @param string $endTime
     *
     * @return OperationalHours
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }
}

