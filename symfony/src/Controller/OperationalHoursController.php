<?php
namespace App\Controller;

use App\Entity\Misc\OperationalHours;
use DoctrineExtensions\Query\Mysql\Time;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class OperationalHoursController extends Controller {
    /**
     * @Route("/list_operational_hours", name="list_operational_hours")
     */
    public function listOperationalHours() {
        $entityManager = $this->getDoctrine()->getManager();
        $operational_hours=$this->getDoctrine()
            ->getRepository(OperationalHours::class)
            ->findAll();
        return $this->render('listOperationalHours.html.twig',array('operationalHours' => $operational_hours));
    }
    /**
     * @Route("/edit_operational_hours", name="edit_operational_hours")
     */
    public function editOperationalHours(Request $request) {
        $edit_id=$request->query->get('id');
        $entityManager = $this->getDoctrine()->getManager();
        $operational_hours= $entityManager->getRepository(OperationalHours::class)->find($edit_id);
        $operationalDay=$operational_hours->getDay();
        $form=$this->createFormBuilder($operational_hours)
            ->add('Day',TextType::class,array('disabled' => true))
            ->add('StartTime',TimeType::class,array(
                'html5' => true,
                'widget' => 'single_text',
                'placeholder' => 'hh:mm'))
            ->add("EndTime",TimeType::class,array(
                'html5' => true,
                'widget' => 'single_text',
                'placeholder' => 'hh:mm'))
            ->add('Submit', SubmitType::class, array('label' => 'Save'))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $operational_hours->setLastModifiedOn(new DateType());
            $entityManager->flush();
            $list_operational_hours= $this->getDoctrine()
                ->getRepository(OperationalHours::class)
                ->findAll();
            return $this->redirectToRoute('list_operational_hours', array('operationalHours'=>$list_operational_hours));
        }
        return $this->render('editOperationalHours.html.twig', array(
            'form' => $form->createView(),
            'operationalDay'=>$operationalDay
        ));
    }
}
