<?php
namespace App\Controller;
use App\Entity\Course\Course;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
class firstController extends Controller {
    /**
     * @Route("/", name="index")
     */ 
    public function index() {
        // replace this line with your own code!
        return $this->render('base.html.twig');
    } 
    
    
    /**
    * @Route("/first", name="first")
    */ 
    public function first(){
    	return $this->render('index.html.twig');
    }
    
     /**
    * @Route("/second", name="second")
    */ 
    public function second(){
    	
    	$courses = $this->getDoctrine()
        ->getRepository(Course::class)
        ->findAll();
    
    	return $this->render('second.html.twig', array('courses'=>$courses));
    }
    
    
}
