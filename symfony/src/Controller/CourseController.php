<?php

namespace App\Controller;

use App\Entity\Course\Course;
use App\Entity\Course\Department;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;



class CourseController extends Controller {

    /**
     * @Route("/listCourses", name="listCourses")
     */
    public function listCourses(){

        $courses = $this->getDoctrine()
            ->getRepository(Course::class)
            ->findAll();

        $unsupportedCourses = $this->getDoctrine()->getRepository(Course::class)
            ->findBy(array('supported' => false,));
        $num = sizeof($unsupportedCourses);

        return $this->render('/course/listCourses.html.twig', array(
            'courses' => $courses,
            'numUnsupported' => $num,
        ));

    }



    /**
     * @Route("/addNewCourse", name="addNewCourse")
     */
    public function addNewCourse(Request $request) {

        // just setup a fresh $course object (remove the dummy data)
        $course = new Course();

        $form = $this->createFormBuilder($course)
            ->add('department', EntityType::class, array(
                // looks for choices from this entity
                'class' => Department::class,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                // uses the Department.name property as the visible option sting
                'choice_label' => 'name',
            ))
            ->add('name', TextType::class, array(
                'label' => 'Course Name',
                'error_bubbling' => true,
            ))
            ->add('number', TextType::class, array(
                'label' => 'Course Number',
                'error_bubbling' => true,
            ))
            ->add('supported', CheckboxType::class, array(
                'label' => 'Is this Course supported',
                'error_bubbling' => true,
                'required' => false))
            ->add('description', TextareaType::class, array(
                'label' => 'Admin Notes',
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Submit'
            ))
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $form->getData() holds the submitted values
            // but, the original `$course` variable has also been updated
            $course = $form->getData();

            $course->setLastModifiedOn()->setCreatedOn();


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($course);
            $entityManager->flush();

            // If all the actions are successful, show course list page;
            return $this->redirectToRoute('listCourses');

        }

        return $this->render('/course/addNewCourse.html.twig', array(
            'form' => $form->createView(),
        ));


    }


    /**
     * @Route("/editCourse", name="editCourse")
     */
    public function editCourse(Request $request) {

        // Here, uses 'GET' METHOD !!
        $id =  $request->query->get('id');

        $entityManager = $this->getDoctrine()->getManager();
        $course = $entityManager->getRepository(Course::class)->find($id);

        if (!$id) {
            throw $this->createNotFoundException(
                'No course found for id '.$id
            );
        }

        $courseName = $course->getName();

        $form = $this->createFormBuilder($course)
            ->add('department', EntityType::class, array(
                // looks for choices from this entity
                'class' => Department::class,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                // uses the Department.name property as the visible option sting
                'choice_label' => 'name',
            ))
            ->add('number', TextType::class, array(
                'label' => 'Course Number',
                'error_bubbling' => true,
            ))
            ->add('name', TextType::class, array(
                'label' => 'Course Name',
                'error_bubbling' => true,
            ))
            ->add('supported', CheckboxType::class, array(
                'label' => 'Is this Course supported',
                'required' => false
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Admin Notes',
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // We just need to UPDATE the Course data, not Department Data.
            // UPDATED Value !!
            // $form->getData() holds the submitted values !!
            // but, the original `$course` variable has also been updated !!
            // $course = $form->getData();
            // JUST Flush Course Data !
            $entityManager->flush();



            // If all the actions are successful, show course list page;
            return $this->redirectToRoute('listCourses');

        }

        return $this->render('/course/editCourse.html.twig', array(
            'form' => $form->createView(),
            'deleteId' => $id,
            'courseName' => $courseName,
        ));

    }


    /**
     * @Route("/deleteCourse", name="deleteCourse")
     */
    public function deleteCourse(Request $request) {

        // Here, uses 'GET' protocol !!
        $id =  $request->query->get('id');

        $entityManager = $this->getDoctrine()->getManager();
        $course = $entityManager->getRepository(Course::class)->find($id);

        if (!$id) {
            throw $this->createNotFoundException(
                'No course found for id '.$id
            );
        }

        //Try-catch block to check if there are any Foreign Key Constraint Violations
        try {
            // Delete Course
            $entityManager->remove($course);
            $entityManager->flush();
        }
        catch(ForeignKeyConstraintViolationException $e)
        {
            //Displaying error message for foreign key constraint violation
            $this->addFlash('error',"This Course has associated data(course number) with Section and cannot be deleted.");
            return $this->redirectToRoute('editCourse',
                array('id' => $id));
        }

        // If all the actions are successful, show course list page;
        return $this->redirectToRoute('listCourses');

    }


}